import path from "path";
import {platform} from "process";

export function filename(url) {
  let __filename = new URL(url).pathname;
  return path.basename(__filename, ".js");
}

export function dirname(url) {
  let __filename = new URL(url).pathname;
  let __dirname = path.dirname(__filename);
  return platform == "win32"
    ? __dirname.slice(1)
    : __dirname;
}