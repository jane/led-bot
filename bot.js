import { createRequire } from 'module';
//import { discover } from './lights/light_parser.js';
//import { contactServer } from './lights/request.js';
const require = createRequire(import.meta.url);

import Eris from 'eris';
import Logger, { levels } from './logger.js';
import CommandParser, { Command } from './parser.js';
import { filename, dirname } from './utils.js';
import path from 'path';
import fs from 'fs';

const cfg = require('./config.json');
if (!fs.existsSync('./whitelist.json')) {
  fs.writeFileSync('./whitelist.json', '{}');
}

const whitelist = require('./whitelist.json');
const dir = dirname(import.meta.url);

const bot = new Eris(cfg.token);

global.ctx = {
  bot: bot,
  log_level: levels[cfg.log_level.toUpperCase()] || levels.WARN,
  set_ctx: (key, value) => {
    global.ctx[key] = value;
  },
  lights: {
    port: cfg.lights_port,
    addr: cfg.lights_address
  }
};
const log = new Logger(filename(import.meta.url), global.ctx.log_level);
const parse = new CommandParser(global.ctx, cfg.prefix || undefined);

const checkUser = (user) => {
  if (cfg.user_whitelist.includes(user.id) || (whitelist.user && whitelist.user.includes(user.id))) {
    return true;
  }
  log.info(`user not on whitelist: ${user.username}`);
  return false;
};
const checkGuild = (guild) => {
  if (cfg.whitelist.includes(guild.id) || (whitelist.guild && whitelist.guild.includes(guild.id))) {
    return true;
  }
  log.info(`guild not on whitelist: ${guild.name}`);
  return false;
};

const saveWhitelist = () => {
  let data = JSON.stringify(whitelist);
  fs.writeFileSync('./whitelist.json', data);
};

global.ctx.whitelist = {
  guild: checkGuild,
  user: checkUser,
  save: saveWhitelist,
  wl: whitelist,
};

bot.on('ready', () => {
  log.info('ready recieved.');
  bot.guilds.forEach((guild) => checkGuild(guild));
});

bot.on('messageCreate', (msg) => parse.parseMsg(msg, global.ctx));

bot.on('error', (err) => log.error(err.toString()));

bot.connect();

async function load_commands() {
  for (let file of files) {
    let p = path.join(cmd_dir, file);
    log.debug(p);
    let obj;
    try {
      obj = await import('file:////' + p);
    } catch (e) {
      log.warn(`loading file ${file}, ran into issue: ${e.stack}`);
      continue;
    }
    if (obj.default != undefined) {
      if (obj.default.constructor.name == 'CommandInitializer') {
        obj.default.initialize(global.ctx);
        let cmds = obj.default.getCommands();
        if (parse.isCmd(cmds)) {
          parse.addCommand(cmds);
        } else if (cmds.constructor.name == 'Array') {
          for (let cmd of cmds) {
            parse.addCommand(cmd);
          }
        }
      }
    } else {
      log.warn('module ' + file + ' returned an undefined module.');
    }
  }
}

let cmd_dir = path.join(dir, 'cmd');
log.debug(dir);
let files = fs.readdirSync(cmd_dir);
load_commands();
// contactServer(discover, (err) => {
//   log.error(err);
// });
