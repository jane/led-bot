import * as ws from "ws";
import * as net from "net";

const hostname = "192.168.1.219";
const port = 29999;

function getBinarySize(string) {
  return Buffer.byteLength(string, "utf8");
}

// export function contactServer(callback, errorCallback) {
//   let client = new net.Socket();

//   let c_port = global.ctx.lights.port || port;
//   let c_addr = global.ctx.lights.addr || hostname;
//   client.connect(c_port, c_addr, () => {
//   });

//   client.on('data', (dat) => {
//     let arr = JSON.parse(dat);
//     callback(arr);
//     client.destroy();
//   })
// }

export default function req(data, callback, errorCallback) {
  let string_data = data;
  let size = getBinarySize(string_data);
  if (size > 9999) {
    errorCallback("too long");
    return;
  }

  let c_port = global.ctx.lights.port || port;
  let c_addr = global.ctx.lights.addr || hostname;
  let url = `ws://${c_addr}:${c_port}`;

  let client = new ws.WebSocket(url);

  client.on("open", () => {
    client.send(string_data);
  });
  client.on("message", (dat) => {
    callback(String(dat));
    client.close();
  });

  client.on("error", (e) => {
    client.terminate();
    errorCallback(e);
  });
}
