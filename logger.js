export const levels = {
	DEBUG: 4,
	INFO: 3,
	WARN: 2,
	ERROR: 1,
	PANIC: 0,
};

export default class Logger {
	constructor(name, level) {
		console.log(`created new logger for ${name} with level ${level}`);
		this.sn(name);
		this.s(level);
	}
	n = 'DEFAULT';
	l = 0;
	sn(n) {
		this.n = n;
	}
	s(l) {
		if (l && l.constructor === Number) {
			this.l = l;
		} else {
			this.l = levels[l];
		}
	}

	lo(l, m) {
		if (l <= this.l) {
			let level = Object.keys(levels).find((key) => levels[key] === l);
			let ms = typeof m == 'object' ? JSON.stringify(m) : m;
			console.log(`${level} [${this.n}]: ${ms}`);
		}
	}

	debug(msg) {
		this.lo(levels.DEBUG, msg);
	}
	info(msg) {
		this.lo(levels.INFO, msg);
	}
	warn(msg) {
		this.lo(levels.WARN, msg);
	}
	error(msg) {
		this.lo(levels.ERROR, msg);
	}
	panic(msg) {
		this.lo(levels.PANIC, msg);
	}
}
