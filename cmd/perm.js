import { CommandInitializer, Command } from '../parser.js';

const initializer = new CommandInitializer();

class WhitelistUser extends Command {
	name = 'wu';
	whitelist = true;
	func = async function (msg, args, ctx) {
		let user = await ctx.bot.users.get(args[0].trim());
		this.log.debug(msg.channel.guild.members);
		this.log.debug(user);
		if (!user) {
			user = msg.channel.guild.members.get(args[0].trim());
			this.log.debug(user);
			if (!user) {
				user = (await msg.channel.guild.fetchMembers({ userIDs: [args[0]] }))[0];
				this.log.debug(user);
			}
		}
		if (user.username) {
			if (!ctx.whitelist.wl) {
				ctx.whitelist.wl = {
					user: [],
					guild: [],
				};
			}
			let list = ctx.whitelist.wl.user || [];
			if (!list.includes(args[0])) {
				list.push(args[0]);
				ctx.whitelist.wl.user = list;
				ctx.whitelist.save();
				msg.channel.createMessage(`added user "${user.username}#${user.discriminator}" (${args[0]}) to whitelist`);
			} else {
				msg.channel.createMessage('user already whitelisted');
			}
		} else {
			msg.channel.createMessage(`user with id ${args[0]} could not be found`);
		}
	};
}
initializer.addCommand(new WhitelistUser());

class WhitelistGuild extends Command {
	name = 'wg';
	whitelist = true;
	func = async function (msg, args, ctx) {
		let guild = await ctx.bot.guilds.get(args[0]);
		if (guild.name) {
			if (!ctx.whitelist.wl) {
				ctx.whitelist.wl = {
					user: [],
					guild: [],
				};
			}
			let list = ctx.whitelist.wl.guild || [];
			if (!list.includes(args[0])) {
				list.push(args[0]);
				ctx.whitelist.wl.guild = list;
				ctx.whitelist.save();
				msg.channel.createMessage(`added guild "${guild.name}" (${args[0]}) to whitelist`);
			} else {
				msg.channel.createMessage('guild already whitelisted');
			}
		} else {
			msg.channel.createMessage(`guild with id ${args[0]} could not be found`);
		}
	};
}

initializer.addCommand(new WhitelistGuild());

class Purge extends Command {
	name = 'purge';
	whitelist = true;
	func = async function (msg, args, ctx) {
		let guilds = await ctx.bot.guilds;
		let whitelist = ctx.whitelist.guild;
		let purged = 0;
		guilds.forEach((guild) => {
			if (!whitelist(guild)) {
				this.log.info(`purging guild ${guild.name} (${guild.id})`);
				guild.leave();
				purged++;
			} else {
				this.log.debug(`keeping guild ${guild.name} (${guild.id})`);
			}
		});
		msg.channel.createMessage(`purged ${purged} guilds.`);
	};
}

initializer.addCommand(new Purge());

export default initializer;
