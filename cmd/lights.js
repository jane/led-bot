import { CommandInitializer, Command } from '../parser.js';
//import parse, { discover, initialize, instructions } from '../lights/light_parser.js';
import req from '../lights/request.js';

const initializer = new CommandInitializer();

class LightsParser extends Command {
  init(ctx, log) {
    this.log = log;
    //initialize(ctx);
  }
  name = 'lights';
  whitelist = true;
  func(msg, args, ctx) {
    if (ctx.sleep) {
      msg.channel.createMessage('i have sleep mode enabled. the lights are probably off.');
      return;
    }
    if (!args.length) {
      msg.channel.createMessage('no args found.');
      return;
    }
    // let instructions = parse(args[0]);

    // this.log.debug(instructions);
    // let res = '```\n';
    // instructions.forEach((instruction) => {
    //   res += JSON.stringify(instruction) + '\n';
    // });
    // res += '```';
    // msg.channel.createMessage(`parsed instructions:\n${res}`);
    req(
      args.join(' '),
      (response) => {
        msg.channel.createMessage(`\`\`\`\n${response}\`\`\``);
      },
      (error) => {
        msg.channel.createMessage(`error: ${error.message}`);
      }
    );
  }
}
initializer.addCommand(new LightsParser());

class Sleep extends Command {
  name = 'sleep';
  whitelist = true;
  func(msg, args, ctx) {
    let sleep = ctx.sleep;
    sleep = !sleep;
    msg.channel.createMessage(`sleep mode is now ${sleep ? 'on' : 'off'}`);
    ctx.set_ctx('sleep', sleep);
  }
}
initializer.addCommand(new Sleep());

export default initializer;
